import requests
import json
import argparse

client_id = ''
client_secret = ''

parser = argparse.ArgumentParser(
    description="~~~~~~~~~~~~~~~~~~~~~~~~~~~~CROWDSTRIKE APIS~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n"
                "Within csmain.py is API functions for Crowd strike. "
                "For each function, you may be required to pass variables and an access token. "
                "To know what to pass through please reference the API documentation found in Crowd strike."
                "https://falcon.crowdstrike.com/support/documentation/46/crowdstrike-oauth2-based-apis"
                ". To use an API follow this format: cs.functionname(variable, accesstoken)",
    epilog="~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~EOF~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
args = parser.parse_args()
def GenToken(client_id,client_secret):
       URL = 'https://api.crowdstrike.com/oauth2/token'
       HEADERS = {
           'Content-Type': 'application/x-www-form-urlencoded',
           'Accept': 'application/json'
       }
       DATA = {
           'client_id':  client_id,
           'client_secret': client_secret
       }
       try:
           result = requests.request("POST", URL, data=DATA, headers=HEADERS)
           result = result.json()
           ACCESS_TOKEN = result['access_token']
           return ACCESS_TOKEN
       except Exception as e:
           error = "web request failed error: " + str(e)
           sys.exit("Check your client secrets.")
           return error
Token = GenToken(client_id,client_secret)
#------Group section ----------------

def GetGroupID(GroupName,ACCESS_TOKEN,raw=False):
    #LOOK INTO A BETTER WAY TO DO THIS. 
        print('Getting group IDs....')
        DATA = {
            'Content-Type': 'application/json',
            }
        HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
              }
        URL = "https://api.crowdstrike.com/devices/combined/host-groups/v1?sort=name.asc"
        result = requests.get( url = URL, headers=HEADERS, data=DATA )
        results = result.json()
        try:
            groups = results['resources']
            for group in groups:
                if group['name'] == GroupName:
                    if raw is True:
                        return results
                    return group['id']
        except:
            error = "Error with getting groupID."
            return error

def GetGroupInfo(GroupID,ACCESS_TOKEN,raw=False):

    DATA = {
            'Content-Type': 'application/json',
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' +  ACCESS_TOKEN
              }
    URL = "https://api.crowdstrike.com/devices/entities/host-groups/v1?ids=" + GroupID
    result = requests.get( url = URL, headers=HEADERS, data=DATA )
    results = result.json()
    return results

# ------------Host Section ------------------
def GetHostID(HostName,ACCESS_TOKEN,raw=False):

    DATA = {
            'Content-Type': 'application/json',
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
              }
    HostName = "'"+HostName+"'"
    URL = "https://api.crowdstrike.com/devices/queries/devices-scroll/v1?filter=hostname:" + HostName

    result = requests.get( url = URL, headers=HEADERS, json=DATA )
    results = result.json()
    try:
        HostAID = results['resources'][0]
        if raw is True:
            return results
        return HostAID
    except:
        error = "Host Not Found."
        return error

def GetHostIDs(HostName,ACCESS_TOKEN):
   # print('Getting Host IDs...')
    DATA = {'Content-Type': 'application/json'}
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' +  ACCESS_TOKEN
              }
    HostName = "'"+HostName+"'"
    URL = "https://api.crowdstrike.com/devices/queries/devices-scroll/v1?filter=hostname:" + HostName
    result = requests.get( url = URL, headers=HEADERS, json=DATA )
    results = result.json()
    try:
        HostAID = results['resources']
        return HostAID
        #return results
    except:
        error = "Host Not Found."
        return error

def GetHostInfo (HostID,ACCESS_TOKEN,raw=False):
    if HostID == "help":
        print("HostID is...")
    else:
        DATA = {
            'Content-Type': 'application/json',
            }
        HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' +  ACCESS_TOKEN
               }

        URL = "https://api.crowdstrike.com/devices/entities/devices/v1?ids=" + HostID

        result = requests.get( url = URL, headers=HEADERS, json=DATA )
        results = result.json()
        return results

def DeleteHosts (IDS, ACCESS_TOKEN):
    DATA = {
            "action_parameters": [
                {
                "name": "string",
                "value": "string"
                }
            ],
            "ids": IDS

    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }


    URL = "https://api.crowdstrike.com/devices/entities/devices-actions/v2?action_name=hide_host"
    result = requests.request("POST", URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results

#-------Editing groups --------------------------#
def EditGroup(HostNames,GroupID,Action,ACCESS_TOKEN,raw=False):
        print('Updating group....')
        print(HostNames)
        HOSTS = json.dumps(HostNames)
        print(HOSTS)
        DATA = {
            "action_parameters": [
                {
                    "name": "filter",
                    "value": "(device_id: "+HOSTS+")"
                }
                ],
                "ids": [
                  GroupID
                  ]
                }
        HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' +  ACCESS_TOKEN
              }
        print(DATA)
        URL = "https://api.crowdstrike.com/devices/entities/host-group-actions/v1?action_name="+Action
        result = requests.request("POST", URL, json=DATA, headers=HEADERS )
        results = result.json()
        return results

def CreateHostGroup(Name, Description, Type, ACCESS_TOKEN,raw=False):
    print('Creating Group....')
    DATA = {
            'Content-Type': 'application/json',
            "resources": [{
                            "name": Name,
                            "description": Description,
                            "group_type": Type
                
                           }]
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' +  ACCESS_TOKEN
              }

    URL = "https://api.crowdstrike.com/devices/entities/host-groups/v1"
    result = requests.request("POST", URL, json=DATA, headers=HEADERS )
    results = result.json()
    return results

def NewDetections (ACCESS_TOKEN,raw=False):

        print('Getting new detections...')
        DATA = {
            'Content-Type': 'application/json',
            }
        HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
               }

        URL = "https://api.crowdstrike.com/detects/queries/detects/v1?filter=status:'new'"
        result = requests.request("GET", URL, data=DATA, headers=HEADERS )
        results = result.json()
        return results

def DetectionInfo (Detection,ACCESS_TOKEN,raw=False):

        print('Getting info on  detection...')
        DATA = {
            'Content-Type': 'application/json',
            "ids": [Detection]
            }
        HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
               }

        URL = "https://api.crowdstrike.com/detects/entities/summaries/GET/v1"
        result = requests.request("POST", URL, json=DATA, headers=HEADERS)
        results = result.json()
        return results

def SlackPost (text,raw=False):

        URL = "https://hooks.slack.com/services/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"
        DATA = {
            'Content-Type': 'application/json',
            'text':text
            }
        HEADERS = {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
              }
        result = requests.request("POST", URL, json=DATA, headers=HEADERS)
        return result

def NewIncidents (ACCESS_TOKEN,raw=False):
    print('Getting new incidents...')
    DATA = {
            'Content-Type': 'application/json',
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
               }

    URL = "https://api.crowdstrike.com/incidents/queries/incidents/v1?filter=status:'40'"
    result = requests.request("GET", URL, data=DATA, headers=HEADERS )
    results = result.json()
    return results

def RTR_Start (HostIDs,ACCESS_TOKEN,BatchID="",raw=False):
    #ISSUE WITH queue_offline_all docs say to send a true var to it not string but this var is not defined ??
    print('Attempting to create RTR BatchID....')
    print(HostIDs)
    DATA = {
             "existing_batch_id":BatchID,
             "host_ids":HostIDs,
             #"queue_offline_all": true
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
               }

    URL = "https://api.crowdstrike.com/real-time-response/combined/batch-init-session/v1?timeout=30&timeout_duration=30s"
    result = requests.request("POST", URL, json=DATA, headers=HEADERS )
    results = result.json()
    return results

def RTR_ActiveResponder(Bcmd, Scmd, HostID, SessionID, ACCESS_TOKEN,raw=False):
    print('Attempting to run command on hosts')
    DATA = {
             "base_command":Bcmd,
             "command_string":Scmd,
             "device_id":HostID,
             "session_id":SessionID
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
               }

    URL = "https://api.crowdstrike.com/real-time-response/entities/active-responder-command/v1"
    result = requests.request("POST", URL, json=DATA, headers=HEADERS )
    results = result.json()
    return results

def RTR_Status(Cid,Sid, ACCESS_TOKEN,raw=False):
    print('Getting cmd result')
    DATA = {
            'Content-Type': 'application/json',
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization': 'Bearer ' + ACCESS_TOKEN
               }

    URL = "https://api.crowdstrike.com/real-time-response/entities/command/v1?cloud_request_id="+Cid+"&sequence_id="+Sid
    result = requests.request("GET", URL, data=DATA, headers=HEADERS)
    results = result.json()
    return results


def DetectionUpdate(DetectionID,AssignTo, comment, ACCESS_TOKEN,raw=False):
    print('Updating Detection.....')
    DATA = {
             "assigned_to_uuid": AssignTo,
             "comment": comment,
             "ids": [DetectionID],
            }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' + ACCESS_TOKEN
               }

    URL = "https://api.crowdstrike.com/detects/entities/detects/v2"
    result = requests.request("PATCH", URL, json=DATA, headers=HEADERS )
    results = result.json()
    return results



def SubmissionID(ACCESS_TOKEN,raw=False):

    #finding sandbox reports....
    print("Getting submission ID's...")

    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/falconx/queries/submissions/v1?limit="
    result = requests.get(url=URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results

# -----------Lifting and containing Hosts section ------------------------
def ContainHost(HostID,ACCESS_TOKEN,raw=False):
    HOST = json.dumps(HostID)
    print("Containing hosts...")
    DATA = {
            "action_parameters": [
                {
                    "name": "HostID",
                    "value": "(device_id: "+HOST+")"
                }
            ],
            "ids": [
                HostID
            ]
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/devices/entities/devices-actions/v2?action_name=contain"

    result = requests.request("POST", URL, headers=HEADERS, json=DATA)
    results = result.json()

    try:
        HostAID = results['ids']
        NoError = "Containment was successful..."
        if raw is True:
            return results
        return HostAID, NoError

    except:
        error = "Containment failure"
        return error

def LiftHost(HostID, ACCESS_TOKEN,raw=False):

    HOST = json.dumps(HostID)
    print(HOST)

    DATA = {
        "action_parameters": [
            {
                "name": "HostID",
                "value": "(device_id: " + HOST + ")"
            }
        ],
        "ids": [
            HostID
        ]
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/devices/entities/devices-actions/v2?action_name=lift_containment"

    result = requests.request("POST", URL, headers=HEADERS, json=DATA)
    results = result.json()
    return results

#-------------------------SANDBOX----------------------
#uploads a file to CS for sandbox (skip for URL)
def SandBoxSubmit (FilePath, ACCESS_TOKEN,raw=False):
    print("Uploading file...")
    FileName = os.path.basename(FilePath)
    DATA = {
            open(FilePath, 'rb').read()

            }
    HEADERS = {
               'Content-Type': 'application/octet-stream',
               'Authorization':'Bearer ' + ACCESS_TOKEN
               }

    URL = "https://api.crowdstrike.com/samples/entities/samples/v2?file_name="+FileName+"&is_confidential=true"
    result = requests.request("POST", URL, json=DATA, data=DATA, headers=HEADERS )
    results = result.json()
    return results

#starts an analysis for Sandbox based on uploaded file/URL
def SandBoxAnalysis (FileWebName, ACCESS_TOKEN, raw=False,FileHash=None, WebURl=None):

    print("Starting Analysis....")
    DATA = {

        "sandbox": [{
            "sha256": FileHash,
            "environment_id": 160,
            "submit_name": FileWebName,
            "url": WebURl
        }]
    }

    HEADERS = {

        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/falconx/entities/submissions/v1"
    result = requests.request("POST", URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results


#checks on the status of an analysis that started
def SandBoxCheckAnalysis(SubmissionID, ACCESS_TOKEN,raw=False):

    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/falconx/entities/submissions/v1?ids=" + SubmissionID
    result = requests.get(url=URL, headers=HEADERS, json=DATA)
    results = result.json()
    return results

#prints off a summary of the analysis report
def AnalysisSummaryInfo(SubmissionID,ACCESS_TOKEN,raw=False):
    print("Getting summary analysis report...")

    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/falconx/entities/report-summaries/v1?ids=" + SubmissionID
    result = requests.get(url=URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results
    
#gives you a full analysis report
def AnalysisFullInfo(SubmissionID, ACCESS_TOKEN,raw=False):
    print("Getting full analysis  report")

    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/falconx/entities/reports/v1?ids=" + SubmissionID
    result = requests.get(url=URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results
    
#reminds you how many private sanboxes can be blown up 
def SubmissionQuota(ACCESS_TOKEN,raw=False):
    print("Getting submission quota...")

    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }
    URL = "https://api.crowdstrike.com/falconx/entities/submissions/v1?ids="

    result = requests.get(url=URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results
    
#gives you the analysisID to any Analysis started (very generic)
def AnalysisID(ACCESS_TOKEN,raw=False):

    #finding sandbox reports....
    print("Getting Sandbox report...")

    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/falconx/queries/reports/v1?limit="
    result = requests.get(url=URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results

#gives you the submission ID to any submission sent to crowdstrike (generic)
def SubmissionID(ACCESS_TOKEN,raw=False):

    #finding sandbox reports....
    print("Getting submission ID's...")

    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/falconx/queries/submissions/v1?limit="
    result = requests.get(url=URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results

# -----------Hiding and unHiding Hosts section ------------------------
def HideHost(HostID,ACCESS_TOKEN,raw=False):
# Works great. Want to work on enhancing...
    print("Deleting host...")
    HOST = json.dumps(HostID)

    DATA = {
            "action_parameters": [
                {
                    "name": "HostID",
                    "value": "(device_id: " + HOST + ")"
                }
            ],
            "ids": [
                HostID
            ]
           }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' +  ACCESS_TOKEN
               }

    URL = "https://api.crowdstrike.com/devices/entities/devices-actions/v2?action_name=hide_host"
    result = requests.request("POST", URL, headers=HEADERS, json=DATA)
    results = result.json()
    return results

def RestoreHost(HostID, ACCESS_TOKEN,raw=False):
# works, but you run into issues with gathering data after its been deleted...
# would need to know the information before you delete
    print("Restoring host...")
    HOST = json.dumps(HostID)
    DATA = {
            "action_parameters": [
                {
                    "name": "HostID",
                    "value": "(device_id: " + HOST + ")"
                }
            ],
            "ids": [
                HostID
            ]
           }
    HEADERS = {
               'Content-Type': 'application/json',
               'Accept': 'application/json',
               'Authorization':'Bearer ' +  ACCESS_TOKEN
               }
    URL = "https://api.crowdstrike.com/devices/entities/devices-actions/v2?action_name=unhide_host"

    result = requests.request("POST", URL, headers=HEADERS, json=DATA)
    results = result.json()
    return results

#pulls the Users UID (was used to assign detections to specific people in crowdstrike from phantom)
def userUID (email, ACCESS_TOKEN,raw=False):
    DATA = {
        'Content-Type': 'application/json',
    }
    HEADERS = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/users/queries/user-uuids-by-email/v1?uid=" + email
    result = requests.get(url=URL, json=DATA, headers=HEADERS)
    results = result.json()
    return results

#RTR SCRIPT UPLOADING
def ScriptGet (ACCESS_TOKEN):
    HEADERS = {
        #'Content-Type': 'multipart/form-data',
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/real-time-response/queries/scripts/v1?filter=name:'test'"
    result = requests.get(url=URL, headers=HEADERS)
    results = result.json()['resources'][0]
    return results

def ScriptReplace(ACCESS_TOKEN):
    FILES = {
        #'Content-Type': 'multipart/form-data',
        'file': ('test1.ps1', open('test1.ps1', 'rb')),
        'id': (None,'60d7f947730411ecb70a16344e8b8c2e_d68652d9ed9e4d9a8e344cb8dcb16205'),
        'description': (None,'test 4 uploading'),
        'name': (None,'test'),
        'comments_for_audit_log':(None, 'uploading this file'),
        'permission_type': (None, 'public'), 
    }
    HEADERS = {
        #'Content-Type': 'multipart/form-data',
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/real-time-response/entities/scripts/v1"
    result = requests.patch(url=URL, files=FILES, headers=HEADERS)
    print(result.content)
    results = result.json()
    return results

def ScriptUpload (ACCESS_TOKEN):
    FILES = {
        #'Content-Type': 'multipart/form-data',
        'file': ('test1.ps1', open('test1.ps1', 'rb')),
        'description': (None,'test 4 uploading'),
        'name': (None,'test'),
        'comments_for_audit_log':(None, 'uploading this file'),
        'permission_type': (None, 'public'), 
    }
    HEADERS = {
        #'Content-Type': 'multipart/form-data',
        'accept': 'application/json',
        'authorization': 'Bearer ' + ACCESS_TOKEN
    }

    URL = "https://api.crowdstrike.com/real-time-response/entities/scripts/v1"
    result = requests.post(url=URL, files=FILES, headers=HEADERS)
    print(result.content)
    results = result.json()
    return results
#r = ScriptUpload(Token)
#r = ScriptGet(Token)
r = ScriptReplace(Token)
print(r)