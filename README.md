# CrowdStrike CLI
    ##############################################                           ..........'......              
    # Tool for using crowdstrike without the GUI #                    '::cc;,'........',;;,,...         
    # Version: Beta 0.01                         #                 .:x00Odc'..;:::,.. ...'.',,,'.       
    # Author: MK                                 #              .;d0XNKxx0d,'oKNNNO,..',,::........     
    # Remember to put in the client secret       #             .lOKXXXKOk0Oko:lx0NKl'....cOd,'''..'.    
    ##############################################            .;clllx0KK0KXXk,..,cddl:;:codlcc:;,'...   
    # USAGE: CS-Host.py <OPTION> <HOSTNAME>      #            .''..':ldxxxkOkoc:,...''''',;:::::;,....  
    #        -c CONTAIN THE HOST                 #            .....';cldkkxdl::::,......'',,;;;;,''.... 
    #        -h PRINT THIS MENU                  #            ..   .,;,:clddo;.....  .......'',;cl:,... 
    #        -i PRINT HOST INFORMATION           #            .     'ccloxOOko,..    .........,cooo:'',.
    #        -l LIFT CONTAINMENT                 #                  .:llodxxkkdoc.    ........;oxdol:''.
    #        -r REAL TIME COMMAND                #                   ,cclooddxxddo;.       ...;oxxl:,.'.
    #                                            #                   .clooddxxxxxdoc.        .;dxdo:',:.
    ##############################################                   .:ooxxkkkkkkxxo:.       .;odxxddoxl
    ##############################################                 ..'lxxkOOOO00OO0Okd:'.    .'codxO0Oxl
    ##############################################        .       ,ldO000000OOOO0KKKXNXK0xl;..,lxk0KKKx;
    ##############################################              .lOKXKK0OOOOOOOOO0KKXXNNNNWNKkxkO00KKKOd
    ##############################################           .;cxOOKKKKKKKOkO000000KKKKKKKXNNNXOkkk0K00x
    ##############################################          ,dkkOOkO0KKKXXKOxk000KKKKKK000KXXNNXOxdkOxl;
    ##############################################        .;xkkkOOOOO00OOXNX0OkkO000O000KKKXXXXXKxlxd'.'
    ##############################################      .;ldxddkkkkOOO0O0KKXXNXK0O0OkOO00KKK00X0o:::'.l0
    ##############################################    ..cddxdldkddkkkkO0kk0KXX0O0K0OkO0000000KXXkl;..,OX
    ##############################################    .,odoodxxdodxxxkkxoxOOOxdk0K00O00OO0OO000K0K0xld00
    ##############################################    ,':odddddoloddddodxkxxkkO00kO000OxxO0OOO000KK0000O
    ##############################################    0o,clclodc;ldddlldxxlokkO0kdk000OkkOKOkO00O0K00OOO


csmain.py is just api function refrences
