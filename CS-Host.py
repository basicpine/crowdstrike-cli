import requests
import json
import sys
import time
import os

os.system("")

class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    WHITE = '\033[37m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Hosts:
    def __init__(self, Hostname):
        #####ADD THE API KEYS HERE####
        self.client_id = ''
        self.client_secret = ''
        ###############################
        
        self.token = Hosts.GenToken(self)['access_token']
        self.HostName = Hostname
        self.HostContained = False
        self.DefaultData =  {'Content-Type': 'application/json'}
        self.DefaultHeaders = {'Content-Type': 'application/json','Accept': 'application/json','Authorization': 'Bearer ' + self.token}
        self.HostID = Hosts.GetHostID(self)
        self.HostInfo = Hosts.GetHostInfo(self)

    def GenToken(self):

        URL = 'https://api.crowdstrike.com/oauth2/token'
        HEADERS = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json'
        }
        DATA = {
            'client_id':  self.client_id,
            'client_secret': self.client_secret
        }

        try:
            result = requests.request("POST", URL, data=DATA, headers=HEADERS)
            result = result.json()
            ACCESS_TOKEN = result['access_token']
            return result

        except Exception as e:
            error = "web request failed error: " + str(e)
            sys.exit("Check your client secrets.")
            return error

    def ApiCall(self, url, req, data=None, headers=None):
        if data == None:
            data = self.DefaultData
        if headers == None:
            headers = self.DefaultHeaders
        baseurl = "https://api.crowdstrike.com"
        URL = baseurl + url
        if req.upper() == "GET":
            result = requests.get(url=URL, headers=headers, data=data)
        if req.upper() == "POST":
            result = requests.request("POST", URL, headers=headers, json=data)
        return result.json()

    def GetHostID(self):
        HostName = "'" + self.HostName + "'"
        url = "/devices/queries/devices-scroll/v1?filter=hostname:" + HostName
        results = Hosts.ApiCall(self, url, 'get')
        try:
            HostAID = results['resources'][0]
            return HostAID
        except:
            # ERROR FUNCTION
            error = "Host Not Found."
            return error

    def GetHostInfo(self):
        url = "/devices/entities/devices/v1?ids=" + self.HostID
        results = Hosts.ApiCall(self, url, 'get')
        return results

    def ContainHost(self):
        data = {
            "action_parameters": [
                {
                    "name": "HostID",
                    "value": "(device_id: " + self.HostID + ")"
                }
            ],
            "ids": [self.HostID]
        }
        url = "/devices/entities/devices-actions/v2?action_name=contain"
        results = Hosts.ApiCall(self, url, 'post', data)
        try:
            HostAID = results['ids']
            NoError = "Containment was successful..."
            self.HostContained = True
            return True
        except:
            error = "Containment failure"
            return error

    def LiftHost(self):
        data = {
            "action_parameters": [
                {
                    "name": "HostID",
                    "value": "(device_id: " + self.HostID + ")"
                }
            ],
            "ids": [
                self.HostID
            ]
        }
        url = "/devices/entities/devices-actions/v2?action_name=lift_containment"
        results = Hosts.ApiCall(self, url, 'post', data)
        self.HostContained = False
        return results


class RTR(Hosts):
    def __init__(self, host, cmd):
        self.SessionID = RTR.Start(self, host)
        self.SessionID
        self.ID = 0
        self.BaseCMD = cmd.split(' ')[0]
        self.CMD = cmd
        self.CloudRequestID = RTR.Admin_Command(self, host)['resources'][0]['cloud_request_id']
        self.Status = RTR.RTR_Admin_Status(self, host)

        while not self.Status['resources'][0]['complete']:
            self.ID += 1
            time.sleep(5)
            self.Status = RTR.RTR_Admin_Status(self, host)

        self.CmdResponse = self.Status['resources'][0]['stdout']
    ########################################################################
    # if statements for special commands. Currently just get is built out. #
    #          Not sure if this should be done here or not...              #
    ########################################################################
        if self.BaseCMD == 'get':
            self.Files = RTR.SessionFiles(self, host)
            for file in self.Files['resources']:
                self.FileName = file['name'].split('\\')[-1]
                self.FileHash = file['sha256']
                RTR.GetFiles(self, host)

    #########################################################################
    def SessionFiles(self, host):
        url = "/real-time-response/entities/file/v1?session_id=" + str(self.SessionID)
        results = Hosts.ApiCall(host, url, 'get')
        return results

    def GetFiles(self, host):
        HEADERS = {
            'Content-Type': 'application/json',
            # 'Accept': 'application/json',
            'Accept': 'application/x-7z-compressed',
            'Authorization': 'Bearer ' + host.token
        }

        URL = "https://api.crowdstrike.com/real-time-response/entities/extracted-file-contents/v1?session_id="+self.SessionID+"&sha256="+self.FileHash+"&filename="+self.FileName
        result = requests.request("GET", URL, headers=HEADERS)
        ZipName = self.FileName.split('.')[0] + ".zip"
        open(ZipName, 'wb').write(result.content)
        return ZipName

    def Start(self, host):
        DATA = {
            "device_id": host.HostID
        }
        HEADERS = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + host.token
        }

        URL = "https://api.crowdstrike.com/real-time-response/entities/sessions/v1"
        result = requests.request("POST", URL, json=DATA, headers=HEADERS)
        results = result.json()
        try:
            SessionID = results['resources'][0]['session_id']
            return SessionID
        except:
            for error in results['errors']:
                print("\n" + Colors.FAIL + error['message'] + Colors.WHITE + "\n")
            sys.exit(Colors.WARNING + "Host might not be online." + Colors.WHITE + "\n")
            return results
        

    def Admin_Command(self, host):
        #   cat, cd, clear, cp, encrypt, env, eventlog, filehash, get, getsid, help, history, ipconfig, kill
        #   ls, map, memdump, mkdir, mount, mv, netstat, ps, put, reg, query, reg set, reg delete
        #   reg load, reg, unload, restart, rm, run, runscript, shutdown, unmap, xmemdump, zip
        #print('Attempting to run command on hosts')
        DATA = {
            "base_command": self.BaseCMD,
            "command_string": self.CMD,
            "device_id": host.HostID,
            "id": self.ID,
            "session_id": self.SessionID
        }
        url = "/real-time-response/entities/admin-command/v1"
        results = Hosts.ApiCall(host, url, 'post', DATA)
        return results

    def RTR_Admin_Status(self, host):
        url = "/real-time-response/entities/admin-command/v1?cloud_request_id=" + self.CloudRequestID + "&sequence_id=0"
        results = Hosts.ApiCall(host, url, 'get')
        return results


if __name__ == "__main__":
    def selected(command, HostName, script=False):
        host = Hosts(HostName)
       
        if host.HostID == 'Host Not Found.':
            sys.exit(Colors.FAIL + 'Host Not Found.'+ Colors.WHITE)

        if command == '-r':
                if script == True:
                    s = str(sys.argv[3])
                    RTRcmd = "runscript -CloudFile=" + s
                    print("Attempting to run %s on %s..." % (s,HostName))
                else:
                    RTRcmd = input(Colors.OKCYAN + "RTR cmd: " + Colors.WHITE)
            
                RTRSession1 = RTR(host,RTRcmd )
                try:
                    print(Colors.OKGREEN + RTRSession1.Status['resources'][0]['stdout'] + Colors.WHITE)
                except:
                    print(Colors.FAIL + json.dumps(RTRSession1.Status, indent = 4) + Colors.WHITE)
    
        elif command == '-c':
            while True:
                check = input(Colors.WARNING + 'Are you sure you want to contain host {}? [y/n]: '.format(HostName)+Colors.WHITE)
                if check.lower() in ('no', 'n'):
                    sys.exit(Colors.FAIL+'User canceled action.'+Colors.WHITE)

                elif check.lower() in ('y', 'yes'):
                    print('containing')
                    host.ContainHost()
                    if host.HostContained == True:
                        print(Colors.OKGREEN+'Host {} Has been contained'.format(HostName)+Colors.WHITE)
                        break
                    else:
                        sys.exit(Colors.FAIL+'error in containment.'+Colors.WHITE)
                else:
                    print('Please enter y or n.')
        elif command == '-l':
            while True:
                check = input(Colors.WARNING+'Are you sure you want to lift containment on host {}? [y/n] '.format(HostName)+Colors.WHITE)
                if check.lower() in ('no', 'n'):
                    sys.exit('User canceled action.')

                elif check.lower() in ('y', 'yes'):
                    print('containing')
                    host.LiftHost()
                    if host.HostContained == False:
                        print(Colors.OKGREEN+'Host {} Has been lifted'.format(HostName)+Colors.WHITE)
                        break
                    else:
                        sys.exit(Colors.FAIL+'error in lifting containment.'+Colors.WHITE)
                else:
                    print('Please enter y or n.')

        elif command == '-i':
            print(Colors.OKGREEN + json.dumps(host.HostInfo, indent = 4) + Colors.WHITE)

        else:
            print("I Ran")
            print('Option not found.')
            HelpMenu()

    def HelpMenu():
        print(Colors.FAIL+"""      
    ##############################################                           ..........'......              
    # Tool for using crowdstrike without the GUI #                    '::cc;,'........',;;,,...         
    # Version: Beta 0.01                         #                 .:x00Odc'..;:::,.. ...'.',,,'.       
    # Author: MK                                 #              .;d0XNKxx0d,'oKNNNO,..',,::........     
    # Remember to put in the client secret       #             .lOKXXXKOk0Oko:lx0NKl'....cOd,'''..'.    
    ##############################################            .;clllx0KK0KXXk,..,cddl:;:codlcc:;,'...   
    # USAGE: CS_Host.py <OPTION> <HOSTNAME>      #            .''..':ldxxxkOkoc:,...''''',;:::::;,....  
    #        -c CONTAIN THE HOST                 #            .....';cldkkxdl::::,......'',,;;;;,''.... 
    #        -h PRINT THIS MENU                  #            ..   .,;,:clddo;.....  .......'',;cl:,... 
    #        -i PRINT HOST INFORMATION           #            .     'ccloxOOko,..    .........,cooo:'',.
    #        -l LIFT CONTAINMENT                 #                  .:llodxxkkdoc.    ........;oxdol:''.
    #        -r REAL TIME COMMAND                #                   ,cclooddxxddo;.       ...;oxxl:,.'.
    #                                            #                   .clooddxxxxxdoc.        .;dxdo:',:.
    ##############################################                   .:ooxxkkkkkkxxo:.       .;odxxddoxl
    ##############################################                 ..'lxxkOOOO00OO0Okd:'.    .'codxO0Oxl
    ##############################################        .       ,ldO000000OOOO0KKKXNXK0xl;..,lxk0KKKx;
    ##############################################              .lOKXKK0OOOOOOOOO0KKXXNNNNWNKkxkO00KKKOd
    ##############################################           .;cxOOKKKKKKKOkO000000KKKKKKKXNNNXOkkk0K00x
    ##############################################          ,dkkOOkO0KKKXXKOxk000KKKKKK000KXXNNXOxdkOxl;
    ##############################################        .;xkkkOOOOO00OOXNX0OkkO000O000KKKXXXXXKxlxd'.'
    ##############################################      .;ldxddkkkkOOO0O0KKXXNXK0O0OkOO00KKK00X0o:::'.l0
    ##############################################    ..cddxdldkddkkkkO0kk0KXX0O0K0OkO0000000KXXkl;..,OX
    ##############################################    .,odoodxxdodxxxkkxoxOOOxdk0K00O00OO0OO000K0K0xld00
    ##############################################    ,':odddddoloddddodxkxxkkO00kO000OxxO0OOO000KK0000O
    ##############################################    0o,clclodc;ldddlldxxlokkO0kdk000OkkOKOkO00O0K00OOO"""+Colors.WHITE+"\n")
        print(Colors.WARNING + """
        *REALTIME SUPPORTED COMMANDS 'get', 'runscript'
            EXAMPLES: 
            get C:\\path\\to\\file 
                WILL DOWNLOAD FILE TO A 7ZIP WITH THE PASSWORD infected.
            
            runscript -CloudFile=<SCRIPTNAME>
            
        
        """+Colors.WHITE)

    commands = ['get', 'runscript']
    options = ['-c', '-h', '-l', '-i', '-t', '-r']

    if len(sys.argv) < 3:
        HelpMenu()
        sys.exit()

    command = sys.argv[1]
    HostName = sys.argv[2]

    if command in options:
        if command == '-h':
            HelpMenu()
            sys.exit()
        
        FileType = HostName.split('.')[-1]
        
        if FileType == 'txt':
            print("SCRIPT MODE")
            #READ FOROM LIST MODE
            with open(HostName,'r') as f:
                Targets = f.readlines()
                print(Targets)
                for host in Targets:
                    print(f"command: {command} Host: {host}")
                    selected(command, host, script=True)

        else:
            selected(command, HostName)
